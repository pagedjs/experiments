class MyHandler extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    afterRendered(pages) {
        // → find the element to populate;
        let wrapper = document.querySelector('.location');
        console.log(wrapper);


        


        // → find the left;
        let usedPageLeft = document.querySelector('#page-4');
        console.log(usedPageLeft);
        
        
        // → right page
        let usedPageRight = document.querySelector('#page-5');
        console.log(usedPageRight);
        
        
        // get the background style
        
        
        
        
        // → clone the elements and set the transformation through css (calcul the ratio)
        
        let clonePageLeft = usedPageLeft.cloneNode('true');
        
        clonePageLeft.style.background = window.getComputedStyle(usedPageLeft).background;

        clonePageLeft.classList.add('clonedLeft');
        wrapper.insertAdjacentElement('beforeend',clonePageLeft);
        
        
        
        
        let clonePageRight = usedPageRight.cloneNode('true');
        clonePageRight.classList.add('clonedRight');

        clonePageRight.style.background = window.getComputedStyle(usedPageRight).background;

        wrapper.insertAdjacentElement('beforeend',clonePageRight);
        
    }
}

Paged.registerHandlers(MyHandler);
