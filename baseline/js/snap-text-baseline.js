function snapBaseline(config){

    const baselineDoc = config.baseline;
    const strict = config.strict;
    const showBaseline = config.showBaseline; 
    const baselineColor = config.baselineColor; 
    const elementBaseline = config.element; 

    /* set the baseline*/
    let setBaseline = ':root{ --pagedjs-baseline: ' + baselineDoc + 'px; }';
    let setBaselineColor = ':root{ --pagedjs-baseline-color: ' + baselineColor + ';}';
    addcss(setBaseline);
    addcss(setBaselineColor);


    /* Visibility of the baseline */
    let showBaselineCSS = elementBaseline + `{
        background: linear-gradient( white 0%, white calc(var(--pagedjs-baseline) - 1px), var(--pagedjs-baseline-color) calc(var(--pagedjs-baseline) - 1px),  var(--pagedjs-baseline-color) var(--pagedjs-baseline)), transparent;
        background-size: 100% var(--pagedjs-baseline);
        background-repeat: repeat-y;
        background-position-y: var(--pagedjs-baseline-position);
    }`

    if(showBaseline) { 
        addcss(showBaselineCSS);   
    }

    /* baseline position + vertical Rythm of paragraphs */
    const paragraph = 'p';
    let baselinePosition = verticalRythm(paragraph, '', '', baselineDoc, strict, 0);
    let baselinePositionCSS = ':root{ --pagedjs-baseline-position : -' + baselinePosition + 'px; }'
    addcss(baselinePositionCSS); 

    /* elements to which the script applies */
    ["p", "h1", "h2", "h3", "h4", "h5", "h6", "ul", "li", "blockquote", "figcaption", "adress", "dt", "dd", "hr", "pre"].forEach(element => snapTextElement(element, baselinePosition, baselineDoc, strict));
}





/* snapTextElement() ________________________________________________________________________________
___________________________________________________________________________________________________
___________________________________________________________________________________________________
*/

function snapTextElement(elementSelector, baselinePosition, baselineDoc, strict) {

    let elem = document.querySelectorAll(elementSelector);
    let configStrict = strict;
    


    // do calculation on each tag element
    verticalRythm(elementSelector, '', '', baselineDoc, configStrict, baselinePosition);


    // do calculation for specific class or id
    for (let i = 0; i < elem.length; i++) {
        let allClasses = elem[i].classList;
        let elemId = elem[i].id;
        if(allClasses.length != 0 || elemId != ""){
            verticalRythm(elementSelector, allClasses, elemId, baselineDoc, configStrict, baselinePosition);

        }
    }


}



/* getStyle() _____________________________________________________________________________________
___________________________________________________________________________________________________
___________________________________________________________________________________________________
*/

function getStyle(elem) {

    let fontFamily = window.getComputedStyle(elem).fontFamily;
    let fontSize = parseInt(window.getComputedStyle(elem).fontSize);
    let lineHeight = parseInt(window.getComputedStyle(elem).lineHeight);
    let fontWeight = parseInt(window.getComputedStyle(elem).fontWeight);
    let marginTop = parseInt(window.getComputedStyle(elem).marginTop);
    let marginBottom = parseInt(window.getComputedStyle(elem).marginBottom);
    let paddingTop = parseInt(window.getComputedStyle(elem).paddingTop);
    let paddingBottom = parseInt(window.getComputedStyle(elem).paddingBottom);
    let borderBottom = parseInt(window.getComputedStyle(elem).borderBottomWidth);
    let borderTop = parseInt(window.getComputedStyle(elem).borderTopWidth);

    let styles = {
        fontFamily: fontFamily,
        fontSize: fontSize,
        fontWeight: fontWeight,
        lineHeight: lineHeight,
        marginTop: marginTop,
        marginBottom: marginBottom,
        paddingTop: paddingTop,
        paddingBottom: paddingBottom,
        borderBottom: borderBottom,
        borderTop: borderTop
    };

    return styles;
}



/* vertivalRythm() ________________________________________________________________________________
___________________________________________________________________________________________________
___________________________________________________________________________________________________
*/

function verticalRythm(elem, elemClass, elemId, base, strict, baselinePosition){
    let baseline = base;
    let configStrict = strict;

    /* render element */
    let element = document.createElement(elem);
    element.setAttribute("id", elemId);
    element.setAttribute("class", elemClass);
    document.body.prepend(element); 

    /* get styles of the font */
    let styles = getStyle(element);

    /* get font metrics (unsing FontMetrics.js) */
    const metrics = FontMetrics({
        fontFamily: styles.fontFamily,
        fontWeight: styles.fontWeight,
        fontSize: styles.fontSize,
        origin: 'baseline'
    })


    /* calculate computed line-height (line-height: normal) of the main font */
    element.innerHTML = "x";
    element.style.lineHeight = "normal";
    element.style.paddingTop = "0px";
    element.style.paddingBottom = "0px";
    element.style.height = "auto";

    computedLineHeight = element.clientHeight;

    /* delete rendered element */
    element.parentNode.removeChild(element);

    /* New line-height of the font (multiple of baseline)*/
    let ceilLineHeight = Math.ceil(styles.lineHeight/baseline);
    let newLineHeight = ceilLineHeight*baseline;

    let newPaddingTop;
    let newPaddingBottom;
    let newMarginTop;
    let newMarginBottom;

    /* New margins */
    let ceilMarginTop = Math.ceil(styles.marginTop/baseline);
    let ceilMarginBottom = Math.ceil(styles.marginBottom/baseline);
    newMarginTop = ceilMarginTop*baseline;
    newMarginBottom = ceilMarginBottom*baseline;

    /* distance between baseline font and baseline document */
    let gapBaseline = ((computedLineHeight + baseline*ceilLineHeight)/2) - (metrics.top * -1 * styles.fontSize);
    let gapBaselineElem = gapBaseline - baselinePosition;

    /* New paggings */
    if(configStrict){

        let sumPaddingTop = styles.paddingTop + styles.borderTop;
        let sumPaddingBottom = styles.paddingBottom + styles.borderBottom;
        let ceilSumTop = Math.ceil(sumPaddingTop/baseline);
        let ceilSumBottom = Math.ceil(sumPaddingBottom/baseline);
        newPaddingTop = ceilSumTop*baseline - styles.borderTop + gapBaselineElem;
        newPaddingBottom = ceilSumBottom*baseline - styles.borderBottom;
    }else{

        let sumPadding = styles.paddingTop + styles.borderTop + styles.paddingBottom + styles.borderBottom; 
        let moduloPadding = sumPadding % baseline;
        if(moduloPadding == 0){
            newPaddingTop = styles.paddingTop + gapBaselineElem;
            newPaddingBottom = styles.paddingBottom;
        }else{
            if (moduloPadding%2 == 0){
                newPaddingTop = styles.paddingTop + (baseline - moduloPadding)/2 + gapBaselineElem;
                newPaddingBottom = styles.paddingBottom + (baseline - moduloPadding)/2;
            }else{
                moduloPadding = moduloPadding - 1;
                newPaddingTop = styles.paddingTop + (baseline - moduloPadding)/2 + gapBaselineElem;
                newPaddingBottom = styles.paddingBottom + (baseline - moduloPadding)/2 - 1;
            }
        }
        
    }


    /* set new paddings and margins for the element */
    if(elemClass == "" && elemId == ""){
        // tag element without class and id
        let elementTag = document.querySelectorAll(elem);
        for(let i = 0; i < elementTag.length; i++) {        
            elementTag[i].style.lineHeight = newLineHeight + "px";
            elementTag[i].style.paddingTop = newPaddingTop + "px";
            elementTag[i].style.paddingBottom = newPaddingBottom + "px";
            elementTag[i].style.marginTop = newMarginTop + "px";
            elementTag[i].style.marginBottom = newMarginBottom + "px";
        }
        
    }else if(elemId == ""){
        // element with class(es)
        let elementSpecificClass = document.getElementsByClassName(elemClass);
        for(let i = 0; i < elementSpecificClass.length; i++) {         
            let elementSpecificId = elementSpecificClass[i].id;
            if(elementSpecificId == "") {
                elementSpecificClass[i].style.lineHeight = newLineHeight + "px";
                elementSpecificClass[i].style.paddingTop = newPaddingTop + "px";
                elementSpecificClass[i].style.paddingBottom = newPaddingBottom + "px";
                elementSpecificClass[i].style.marginTop = newMarginTop + "px";
                elementSpecificClass[i].style.marginBottom = newMarginBottom + "px";
            }
        }
    }else{
        // element with id
        let elementSpecificId = document.getElementById(elemId);
        elementSpecificId.style.lineHeight = newLineHeight + "px";
        elementSpecificId.style.paddingTop = newPaddingTop + "px";
        elementSpecificId.style.paddingBottom = newPaddingBottom + "px";
        elementSpecificId.style.marginTop = newMarginTop + "px";
        elementSpecificId.style.marginBottom = newMarginBottom + "px";
    }

    /* return for the baseline position */
    return gapBaseline;

}




/* addcss() ________________________________________________________________________________
___________________________________________________________________________________________________
___________________________________________________________________________________________________
*/

function addcss(css){
    let head = document.getElementsByTagName('head')[0];
    let s = document.createElement('style');
    s.setAttribute('type', 'text/css');
    if (s.styleSheet) {   // IE
        s.styleSheet.cssText = css;
    } else {// the world
        s.appendChild(document.createTextNode(css));
    }
    head.appendChild(s);
}
