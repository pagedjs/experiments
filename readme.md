









Below, a short description of the experiments you can find in this repo.

\+\+\+ Usable \
\+\+ Experimental, be careful when you use it \
\-\- Very experimental, use only if you're confortable with paged.js \
\-\-\- Doesn't work, needs to be rewritten


## Baseline (---)

The script aligns text content to a baseline grid. First it generates the baseline, then it sets a new margin and line-height to align the text elements to the next superior multiple of the baseline.

Nota bene : 

- The script needs to access the [FontMetrics](https://github.com/soulwire/FontMetrics/blob/master/source/FontMetrics.js) script made by [@soulwire](https://github.com/soulwire/FontMetrics/blob/master/source/FontMetrics.js).

- The script only works for horizontal writing. 
- Non-text block elements can break the vertical rythm. 
- it's not a polyfill for the W3C's working drafts [CSS Line Grid Module Level 1](https://drafts.csswg.org/css-line-grid/) and [CSS Rhythmic Sizing](https://drafts.csswg.org/css-rhythm-1/)



## Bleeds (+++)

The script adds bleeds, cropmarks and trim for printing. **Already implemented in paged.js** 



## Book-index (+++)

The script generates a book index. The book index lists the key-word(s) of your choice from the book and point to the page the reader can find more information about it. As an example, the book index lists neologisms and links to their definition in the book. Wrap the key-word(s) with a `<span class="book-index">` and the data attribute `data-book-index=""` with the word(s) you want to appear in the book index. Also, add the book-index element somewhere in your HTML.

Nota bene :

- You can define an alphabetical order (or not) in the book-index
- The script needs to be generated before paged.js

See pagedjs.org post: [Build An Index With Pagedjs](https://www.pagedjs.org/posts/2020-02-16-buildanindexwithpagedjs/)


## Chunker-add-page (--)

Scripts to add full page image in the content. 

The repo contains several scripts and CSS code that cab be added to put an element in full page or an image in double page (works only with image elements for double page). The scripts have to be merged and still need some work.

## ColumnsAndFloat (+++)

In a layout using css columns, floating quotes and snap images, the script determines the position of the quotes (left or right column) and add the corresponding class to help the management of the floating in the CSS.  

- The script is writen for double columns only
- the `snapImages.js` file is in the /js/ folder but isn't used here.



## Drop-cap (+++)

This script adds dropcaps in the text. You can calculate the number of lines the drop capped letter should occupy and choose its font size.



## Endnotes (++)

The script sets endnotes at the end of each chapter. It numbers the note calls and the endnotes by chapter and add a return to the note call at the end of each endnote.

Nota Bene :

- You need to add the endnote function in the `beforeParsed(content){}` handler. 
- The script looks for footnotes with an id starting by "fn-"  



## Margin-notes (++)

The script creates marginal notes from `span` elements and move notes if notes overlapping or if the last note overflow at the end of the page. Wrap the marginal notes with a `<span class="margin-note">`.  The script transforms all `.margin-note`.

Nota Bene :

- You have 4 ways to place the notes : outside, inside, left and right
- If you want to change the CSS `width` value, add `!important` at the end, as the script is not directly integrated to paged.js
- BE CAREFUL: It's not possible for paged.js to break the note into 2 pages yet. If you have to many note on a page, the notes overflow.



## Notes-float (--)

The script uses `float` for notes.



## OrderedList (+++)

This script allows ordered lists running in multiple pages. Before paged.js parse the content, the script stores the number of each list-element in a data attribute to preserve it in the split.



## Repeating-table-header (++)

The script clones the `thead` of a splited table to repeat it in the next page.

Nota bene :

- Does it work with table running in more than one page ? For example with a glossary 



## Sidenotes (--)

The script adds sidenotes in the book using the class `sidenote`.

Nota bene :

- BE CAREFUL: It's not possible for paged.js to break the note into 2 pages yet. If you have to many note on a page, the notes overflow.


## SmartQuotes (+++)

The script automatically converts neutral quotation marks to typographic ones (doubles and single) , plus it converts `--` to quotation dash.



## Table-of-content (+++)

The script generate a table of content from an array of elements. In the array, call the elements you want to see in the table of content by their type or class. Add a `.tocElement` where you want your table of content to go in your HTML. The script also add the corresponding page numbers.

See pagedjs.org post: [Build a Table of Contents from your HTML](https://www.pagedjs.org/posts/2020-02-19-toc/)