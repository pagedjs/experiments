// FULLPAGE variables ----------------------------------------
var classElemFullPage = "full-page";
var spreadElement = "spread";

// -----------------------------------------------------------


class chunckerAddPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.fullPageEls = [];
    this.spreadEls = [];
    this.token;
  }



  renderNode(clone, node) {

    // if you find a full page element, move it in the array
    if (node.nodeType == 1 && node.classList.contains(classElemFullPage)) {
      this.fullPageEls.push(clone);

      // remove the element from the flow by hiding it.
      clone.style.display = "none";
    }

    // if you find a spread page element, move it in the array

    if (node.nodeType == 1 && node.classList.contains(spreadElement)) {

      clone.classList.remove("spread");
      clone.classList.add("spreadProcess", "left");

      const rightElement = document.createElement(node.tagName);
      rightElement.classList.add("spreadProcess", "right");
      rightElement.innerHTML = clone.innerHTML;


      this.spreadEls.push(clone);
      this.spreadEls.push(rightElement);


      // remove the element from the flow by hiding it.
      clone.style.display = "none";
    }


  }

  afterPageLayout(pageElement, page, breakToken, chunker) {
    // if there is an element in the fullPageEls array, 

    while (this.fullPageEls.length) {
      // put the first element on the page
      let fullPage = chunker.addPage();
      fullPage.element.querySelector(".pagedjs_page_content").insertAdjacentElement('afterbegin', this.fullPageEls[0]);
      fullPage.element.classList.add("pagedjs_page_full");
      this.fullPageEls[0].style.removeProperty("display");
      this.fullPageEls.shift();
    }

    //  spread images'
    while (this.spreadEls.length) {
      let fullPage = chunker.addPage();

      if (fullPage.element.dataset.pageNumber % 2 == 0) {
        fullPage.element.classList.add("spreadLeft");

        fullPage.element.querySelector(".pagedjs_page_content").insertAdjacentElement('afterbegin', this.spreadEls[0]);
        this.spreadWrap = true;
        this.spreadEls[0].style.removeProperty("display");
        this.spreadEls.shift();

        continue;
      }

      if (fullPage.element.dataset.pageNumber % 2 == 1) {
        fullPage.element.classList.add("spreadRight");

        fullPage.element.querySelector(".pagedjs_page_content").insertAdjacentElement('afterbegin', this.spreadEls[0]);
        this.spreadEls[0].style.removeProperty("display");

        this.spreadEls.shift();

        continue;
      }
    }

  }

}
Paged.registerHandlers(chunckerAddPage);