/* createNoteElement _______________________________________________________________
____________________________________________________________________________________*/

function createNoteElements(elem){
    let notes = document.getElementsByClassName(elem); 
  
    for(let i = 0; i < notes.length; i++) {
        let note = notes[i];
        note.classList.add("pagedjs_note");
		note.style.position = "absolute";
		
		let callNote = document.createElement("span");
		callNote.className = 'pagedjs_callnote';
		note.parentNode.insertBefore(callNote, note);
    }

}


/* registerHandlerNote _______________________________________________________________
______________________________________________________________________________________*/

function registerHandlerNote(){

	class notesFloat extends Paged.Handler {
		constructor(chunker, polisher, caller) {
			super(chunker, polisher, caller);
		}
		afterRendered(pages) {
			makeNoteArea({
				notesAreaPageLeft: 'bottom left',
				notesAreaPageRight: 'bottom right'
			});
		
		}
	}
	Paged.registerHandlers(notesFloat);

}


/* addStyleNotesArea _______________________________________________________________
____________________________________________________________________________________*/

function addStyleNotesArea(notesArea){

	switch (notesArea.notesAreaPageLeft) {
		case 'bottom':
		case 'bottom left':
		case 'bottom outside':
			let cssLeftBottom = '.pagedjs_left_page .pagedjs_area-notes{ width: var(--pagedjs_notes-area-page-left); float: left; clear: both; } .pagedjs_left_page .pagedjs_area-notes-hackBottom{ width: 0px; float: left; clear: both; }';
			addcss(cssLeftBottom);
			break;
		case 'bottom right':
		case 'bottom inside':
			let cssLeftBottomRight = '.pagedjs_left_page .pagedjs_area-notes{ width: var(--pagedjs_notes-area-page-left); float: right; clear: both; } .pagedjs_area-notes-hackBottom{ width: 0px; float: right; clear: both; }';
			addcss(cssLeftBottomRight);
			break;
		case 'top':
		case 'top left':
		case 'top outside':
			let cssLeftTop = '.pagedjs_left_page .pagedjs_area-notes{ width: var(--pagedjs_notes-area-page-left); float: left; clear: both; }';
			addcss(cssLeftTop);
			break;
		case 'top right':
		case 'top inside':
			let cssLeftTopRight = '.pagedjs_left_page .pagedjs_area-notes{ width: var(--pagedjs_notes-area-page-left); float: right; clear: both; }';
			addcss(cssLeftTopRight);
			break;
		default:
			let cssLeftDefault = '.pagedjs_left_page .pagedjs_area-notes{ width: var(--pagedjs_notes-area-page-left); float: left; clear: both; } .pagedjs_area-notes-hackBottom{ width: 0px; float: left; clear: both; }';
			addcss(cssLeftDefault);
	}
	

	switch (notesArea.notesAreaPageRight) {
		case 'bottom':
		case 'bottom left':
		case 'bottom inside':
			let cssRightBottom = '.pagedjs_right_page .pagedjs_area-notes{ width: var(--pagedjs_notes-area-page-right); float: left; clear: both; } .pagedjs_right_page .pagedjs_area-notes-hackBottom{ width: 0px; float: left; clear: both; }';
			addcss(cssRightBottom);
			break;
		case 'bottom right':
		case 'bottom outside':
			let cssRightBottomRight = '.pagedjs_right_page .pagedjs_area-notes{ width: var(--pagedjs_notes-area-page-right); float: right; clear: both; } .pagedjs_right_page .pagedjs_area-notes-hackBottom{ width: 0px; float: right; clear: both; }';
			addcss(cssRightBottomRight);
			break;
		case 'top':
		case 'top left':
		case 'top inside':
			let cssRightTop = '.pagedjs_right_page .pagedjs_area-notes{ width: var(--pagedjs_notes-area-page-right); float: left; clear: both; }';
			addcss(cssRightTop);
			break;
		case 'top right':
		case 'top outside':
			let cssRightTopRight = '.pagedjs_right_page .pagedjs_area-notes{ width: var(--pagedjs_notes-area-page-right); float: right; clear: both; }';
			addcss(cssRightTopRight);
			break;
		default:
			let cssRightDefault = '.pagedjs_right_page .pagedjs_area-notes{ width: var(--pagedjs_notes-area-page-right); float: left; clear: both; } .pagedjs_right_page .pagedjs_area-notes-hackBottom{ width: 0px; float: left; clear: both; }';
			addcss(cssRightDefault);
	}
}




/* makeNoteArea _______________________________________________________________
____________________________________________________________________________________*/

function makeNoteArea(config){

	addStyleNotesArea(config);
		
	let pages = document.getElementsByClassName('pagedjs_page');
	let classPageLeft = "pagedjs_left_page";
	let classPageRight = "pagedjs_right_page";
	

	for (let p = 0; p < pages.length; p++) {

		
		const page = pages[p];
		let pageContentTrue = page.getElementsByClassName('pagedjs_page_content')[0] 
		//console.log(page.getElementsByClassName('pagedjs_page_content')[0]);

		if(pageContentTrue.lenght != '0') {
		/* don't work */
			
			let contentPage = page.getElementsByClassName('pagedjs_page_content')[0];

			/* create notes areas */
			
			let target = contentPage.children[0];

			let notesAreaHack = document.createElement("div");
			notesAreaHack.className = 'pagedjs_area-notes-hackBottom';
			target.parentNode.insertBefore(notesAreaHack, target);

			let notesArea = document.createElement("div");
			notesArea.className = 'pagedjs_area-notes';
			target.parentNode.insertBefore(notesArea, target);


			/* Move notes in notes area */

			let notes = page.getElementsByClassName('pagedjs_note');
			
			for (let i = 0; i < notes.length; i++) {

				let note =  notes[i];
				let area = page.getElementsByClassName('pagedjs_area-notes')[0];
				let areaBottomHack = page.getElementsByClassName('pagedjs_area-notes-hackBottom')[0];
				
				area.appendChild(note);
				note.style.position = "relative";
				
				if(page.classList.contains(classPageLeft)){
					if(
						config.notesAreaPageLeft == 'top' 
						|| config.notesAreaPageLeft == 'top left'
						|| config.notesAreaPageLeft == 'top right'
						|| config.notesAreaPageLeft == 'top inside'
						|| config.notesAreaPageLeft == 'top outside'
					){				
						areaBottomHack.style.height = '0px';
					}else{
						areaBottomHack.style.height = contentPage.offsetHeight - area.offsetHeight - 1 + 'px';	
						// need redo chunker
					}
					
				}else{
					if(
						config.notesAreaPageRight == 'top' 
						|| config.notesAreaPageRight == 'top left'
						|| config.notesAreaPageRight == 'top right'
						|| config.notesAreaPageRight == 'top inside'
						|| config.notesAreaPageRight == 'top outside'
					){				
						areaBottomHack.style.height = '0px';
					}else{
						areaBottomHack.style.height = contentPage.offsetHeight - area.offsetHeight - 1 + 'px';	
						// need redo chunker
					}
				}

			}/* for notes*/

		}

	}/* for pages*/

}



function addcss(css){
    var head = document.getElementsByTagName('head')[0];
    var s = document.createElement('style');
    s.setAttribute('type', 'text/css');
    if (s.styleSheet) {   // IE
        s.styleSheet.cssText = css;
    } else {// the world
        s.appendChild(document.createTextNode(css));
    }
    head.appendChild(s);
}


