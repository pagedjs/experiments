
# Snap text baseline

A script for aligning text content to a baseline grid.   

Warning:
* This script works only with horizontal writings.
* If you create block element (block, images, figure, etc.) whose height is not a multiple of your defined baseline, that can break the vertical rhythm.
* it's not a polyfill for the W3C's working drafts [CSS Line Grid Module Level 1](https://drafts.csswg.org/css-line-grid/) and [CSS Rhythmic Sizing](https://drafts.csswg.org/css-rhythm-1/)

## How to use

Add the main script : 
```
<script src="js/snap-text-baseline.js" type="text/javascript"></script>
```

The script need to have access to the metrics of the font, for this, it uses the [FontMetrics](https://github.com/soulwire/FontMetrics/blob/master/source/FontMetrics.js) script made by [@soulwire](https://github.com/soulwire/FontMetrics/blob/master/source/FontMetrics.js). You can find this script on this repo to. 
Add the FontMetrics script in your document: 
```
<script src="js/FontMetrics.js" type="text/javascript"></script>
```

After, add this code in the `head` of you html document
```javascript
<script>
    window.onload = function(){
        snapBaseline({
            baseline: 40,
            strict: true,
            showBaseline: true,
            element: 'body',
            baselineColor: '#bfbfbf'
        });
    }
</script>
```


### Configuring the script
`baseline`: set the size of the baseline in pixel.  
`strict`: with `true`, all the text strictly snap on the baseline. It's happend you dosen't want that the text exactly snap on the baseline and keep the proportion padding you define, so if you declare `false`, the text will not be strictly snap to the baseline but will keep the vertical rhythm related to the baseline (the difference in calculation between the two possibilities is explained below).  
`showBaseline`: define the visibility of the baseline, with `true` the baseline is visible and with `false` the baseline is hidden.  
`element`: define the element where the baseline applies.
`baselineColor`: obviously, the color of you baseline

### Use the script with paged.js

With paged.js, the calculation of the the baseline need to be done before that paged.js fragmented the content into pages. Use the hook `before` to call the script:

```javascript
<script>
    window.PagedConfig = {
        before: function() {
        snapBaseline({
            baseline: 40,
            strict: true,
            showBaseline: true,
            element: '.pagedjs_page',
            baselineColor: '#bfbfbf'
        });
        }
    };
</script>
```

Additionnaly, add the `async` property to the paged.js script:
```javascript
<script async src="http://localhost:9090/dist/paged.polyfill.js"></script>
```

Note: for the text to be exactly aligned with the visible baseline, the margin top of each page must be a multiple of the baseline size.



## What the script do ?

This script align text content to invisible grids in the document. It applies to the entire document, more specifically, on the following elements:  
`p`, `h1`, `h2`, `h3`, `h4`, `h5`, `h6`, `ul`, `li`, `blockquote`, `figcaption`, `adress`, `dt`, `dd`, `hr`, `pre`

First, the script create the baseline:
* set a baseline in css with the background properties on the selected element (`element` in the configuration)
* render a `p` without class or id, considering like the main text
* calculate the baseline position in fonction of this `p` element

After, each elements are snap to the baseline with the following calculations:
- acces to the metric of the font to calculate the position of the baseline of each font and snap it on the main baseline of the document by adding padding-top (let's call it here the "gap").
- set the new line-height of the element for a superior multiple of the baseline according to the line-height of the element (* see "more about CSS line-height")
- set the new margin top and the new margin bottom in the same way to avoid problem with margin collapse (* see "note about margin collapse")
- set new paddings in two ways:
    * if `strict: true`: get the sum of padding top + border top and set a new padding-top so that this sum is equal to a superior multiple of the baseline + the gap, same with the sum of padding bottom + border bottom
    * if `strict: false`: get the sum of padding top + border top + padding-bottom + border-bottom, calculate superiotr multiple of the baseline, divides the result by 2 and adds the new result to padding-top and padding bottom, adds also the gap in padding-top


### (*) Note about margin collapse

Collapsing margins happen when two vertical margins come in contact with one another. If one margin is greater than the other, then that margin overrides the other, leaving one margin.

With the merging of margins, it is more important to consider that for an element:
1. the upper margin corresponds to the minimum withdrawal desired before the element;
2. the lower margin corresponds to the minimum withdrawal desired after the element.

Note that this collapse also applies to the parent and child elements

To know more, see:
* [https://css-tricks.com/what-you-should-know-about-collapsing-margins/](https://css-tricks.com/what-you-should-know-about-collapsing-margins/)
* [https://jonathan-harrell.com/whats-the-deal-with-margin-collapse/](https://jonathan-harrell.com/whats-the-deal-with-margin-collapse/)
* [https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Mastering_margin_collapsing](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Mastering_margin_collapsing)


### (*) More about CSS line-height

The CSS line-height is a complex CSS properties. To know more about it:
* [https://iamvdo.me/en/blog/css-font-metrics-line-height-and-vertical-align](https://iamvdo.me/en/blog/css-font-metrics-line-height-and-vertical-align)
* [https://www.w3.org/TR/CSS2/visudet.html#line-height](https://iamvdo.me/en/blog/css-font-metrics-line-height-and-vertical-align)
* [https://christopheraue.net/design/vertical-align](https://iamvdo.me/en/blog/css-font-metrics-line-height-and-vertical-align)