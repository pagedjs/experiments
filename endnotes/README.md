# Script for endnotes

A little script to set endnotes in Paged.js:
- Exemple of structure for endnotes in each chapters,
- Possibility to have multiple elements in one note,
- Numbers note calls and endnotes by chapter,
- Adds a return to the note call at the end of each endnote.

The endnote function need to be register in the ` beforeParsed(content){}` handler.