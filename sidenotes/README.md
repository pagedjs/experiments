# Sidenotes script

A little script to do sidenotes in paged.js. Note that it's on experiment.  

1. Download and call the script in the `<head>` of your document

```js
<script src="sidenotes.js" type="text/javascript"></script>
```

2. Define the sidenotes in HTML (use the class `sidenote`:

```html
<p>...felis ut purus.<span class="appelnote"></span><span class="sidenote">The text of my sidenote.</span> Morbi cursus ...</p>
```


The script need some CSS to work:


3. Set position absolute of sidenote (so that they are not taken into account in the fragmentation of pages):

```CSS
.sidenote {
	display: block;
	position: absolute;
}
```



4. Define the position and the size of sitenote areas

```CSS
.area-sidenote-left {
	width: 150px;
	position:absolute;
	top: var(--margin-top);
}

.area-sidenote-right {
	width: 150px;	
	position:absolute;
    top: var(--margin-top);
	left: calc(var(--width) - var(--margin-right));
}
```

5. Use the CSS counters

```CSS
body { counter-reset: appelnote sidenote; }

.appelnote { counter-increment: appelnote; }
.appelnote:before { content: counter(appelnote); }

.sidenote { counter-increment: sidenote; }
.sidenote:before { content: counter(sidenote) ". "; }
```