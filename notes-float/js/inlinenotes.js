function inlinenotes(elem){
    let notesCall = document.getElementsByClassName(elem);

	for(let i = 0; i < notesCall.length; ++i){
       console.log(notesCall);
        let noteCall = notesCall[i];
       // console.log(noteCall);
        let idNote = noteCall.href.split("#")[1];
        
        let span = document.createElement("span");
        span.className = 'sidenote';
        span.id = 'span-' + idNote;
        noteCall.parentNode.insertBefore(span, noteCall);
        noteCall.parentNode.removeChild(noteCall);
        
        let noteMarker = document.getElementById(idNote);
        let noteContent = noteMarker.parentNode.innerHTML;
        span.innerHTML = noteContent;

        let noteMarkerSpan = document.getElementById(idNote);
        document.getElementById(idNote).parentNode.removeChild(noteMarkerSpan);
		
    }
    
    let listNote = document.getElementById('list-footnote');
    listNote.parentNode.removeChild(listNote);

}


