// spread variables ----------------------------------------
var classElemFullPage = "full-page";

class fullPageStuff extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.fullPageEls = new Set();
    this.usedPagedEls = new Set();
  }

  /* add figure ID */

  // beforeParsed(content) {
  //   let figures = content.querySelectorAll("figure");
  //   for (let i = 0; i < figures.length; i++) {
  //     let numId = i + 1;
  //     figures[i].id = "figure-" + numId;
  //   }
  // }

  renderNode(clone, node) {
    // if you find a full page element, move it in the array
    if (node.nodeType == 1 && node.classList.contains(classElemFullPage)) {
		console.log(node);
		this.fullPageEls.add(node);
      	this.usedPagedEls.add(node);

      // remove the element from the flow by hiding it.
      clone.style.display = "none";
    }
  }

  afterPageLayout(pageElement, page, breakToken, chunker) {
    // if there is an element in the fullPageEls Set, (goodbye arrays!)

    for (let img of this.fullPageEls) {
      if(page.element.classList.contains("pagedjs_right_page")) {
        
        let imgRight = img.cloneNode(true);

      // put the first element on the page
    //   if (!this.usedPagedEls.has(img)) {
        let fullPage = chunker.addPage();
        fullPage.element
          .querySelector(".pagedjs_page_content")
          .insertAdjacentElement("afterbegin", img);
        fullPage.element.classList.add("pagedjs_page_fullLeft");

        // page right
        let fullPageRight = chunker.addPage();
        fullPageRight.element
          .querySelector(".pagedjs_page_content")
          .insertAdjacentElement("afterbegin", imgRight);
        fullPageRight.element.classList.add("pagedjs_page_fullRight");
        // img.style.removeProperty("display");
        img.style.removeProperty("display");


        console.log(this.fullPageEls);
        console.log(this.usedPagedEls);

        this.fullPageEls.delete(img);
    //   }
    }
  }
  }
}
Paged.registerHandlers(fullPageStuff);
